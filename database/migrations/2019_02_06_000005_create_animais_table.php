<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnimaisTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'animais';

    /**
     * Run the migrations.
     * @table animais
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->unsignedInteger('tipo_animal_id');
            $table->unsignedInteger('lotes_id');
            $table->float('quantidade')->nullable();
            $table->float('peso_total')->nullable();

            $table->index(["lotes_id"], 'fk_animais_lotes1_idx');

            $table->index(["tipo_animal_id"], 'fk_animais_tipo_animal_idx');


            $table->foreign('tipo_animal_id', 'fk_animais_tipo_animal_idx')
                ->references('id')->on('tipo_animal')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('lotes_id', 'fk_animais_lotes1_idx')
                ->references('id')->on('lotes')
                ->onDelete('no action')
                ->onUpdate('no action');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
