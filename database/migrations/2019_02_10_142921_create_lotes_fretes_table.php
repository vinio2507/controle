<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLotesFretesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lotes_fretes', function (Blueprint $table) {
            $table->unsignedInteger('lotes_id')->nullable();
            $table->float('distancia')->nullable();
            $table->unsignedInteger('tipo_id')->nullable();
            $table->integer('animais')->nullable();
            $table->float('valor')->nullable();
            $table->timestamps();

            $table->foreign('lotes_id', 'lotes_id_foreign_key')->references('id')->on('lotes');
            $table->foreign('tipo_id', 'tipo_frete_id_forign_key')->references('id')->on('tipo_frete');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lotes_fretes');
    }
}
