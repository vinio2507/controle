<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLotesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'lotes';

    /**
     * Run the migrations.
     * @table lotes
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('numero');
            $table->float('valor_arroba');
            $table->float('comissao')->nullable();
            $table->string('proprietario', 200)->nullable();
            $table->float('distancia')->nullable();
            $table->float('valor_frete')->nullable();
            $table->float('quantidade_caminhao', 45)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
