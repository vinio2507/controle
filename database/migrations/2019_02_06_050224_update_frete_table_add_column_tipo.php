<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateFreteTableAddColumnTipo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('fretes', 'tipo')) {
            Schema::table('fretes', function (Blueprint $table) {
                $table->unsignedInteger('tipo')->nullable();

                $table->foreign('tipo', 'tipo_id_foreign_key')->references('id')->on('tipo_frete');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('fretes', 'tipo')) {
            Schema::table('fretes', function (Blueprint $table) {
                $table->dropForeign('tipo_id_foreign_key');
                $table->dropColumn('tipo');
            });
        }
    }
}
