<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableTipoFreteAddQuantidadeAnimaisColunm extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('tipo_frete', 'quantidade_animais')) {
            Schema::table('tipo_frete', function (Blueprint $table) {
                $table->integer('quantidade_animais')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('tipo_frete', 'quantidade_animais')) {
            Schema::table('tipo_frete', function (Blueprint $table) {
                $table->dropColumn('quantidade_animais');
            });
        }
    }
}
