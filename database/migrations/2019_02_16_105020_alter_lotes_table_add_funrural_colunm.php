<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLotesTableAddFunruralColunm extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('lotes', 'funrural')) {
            Schema::table('lotes', function (Blueprint $table) {
                $table->float('funrural')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('lotes', 'funrural')) {
            Schema::table('lotes', function (Blueprint $table) {
                $table->dropColumn('funrural');
            });
        }
    }
}
