<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLotesTableDropColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('lotes', 'distancia')) {
            Schema::table('lotes', function (Blueprint $table) {
                $table->dropColumn('distancia');
            });
        }

        if (Schema::hasColumn('lotes', 'quantidade_caminhao')) {
            Schema::table('lotes', function (Blueprint $table) {
                $table->dropColumn('quantidade_caminhao');
            });
        }

        if (Schema::hasColumn('lotes', 'comissao_arroba')) {
            Schema::table('lotes', function (Blueprint $table) {
                $table->dropColumn('comissao_arroba');
            });
        }

        if (Schema::hasColumn('lotes', 'tipo_frete_id')) {
            Schema::table('lotes', function (Blueprint $table) {
                $table->dropForeign('tipo_frete_id_foreign');
                $table->dropColumn('tipo_frete_id');
            });
        }

        if (!Schema::hasColumn('lotes', 'total_animais')) {
            Schema::table('lotes', function (Blueprint $table) {
                $table->integer('total_animais')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasColumn('lotes', 'distancia')) {
            Schema::table('lotes', function (Blueprint $table) {
                $table->float('distancia');
            });
        }

        if (!Schema::hasColumn('lotes', 'quantidade_caminhao')) {
            Schema::table('lotes', function (Blueprint $table) {
                $table->float('quantidade_caminhao');
            });
        }

        if (!Schema::hasColumn('lotes', 'comissao_arroba')) {
            Schema::table('lotes', function (Blueprint $table) {
                $table->float('comissao_arroba');
            });
        }

        if (!Schema::hasColumn('lotes', 'tipo_frete')) {
            Schema::table('lotes', function (Blueprint $table) {
                $table->unsignedInteger('tipo_frete_id')->default(1);
                $table->foreign('tipo_frete_id','tipo_frete_id_foreign')->references('id')->on('tipo_frete');
            });
        }

        if (Schema::hasColumn('lotes', 'total_animais')) {
            Schema::table('lotes', function (Blueprint $table) {
                $table->dropColumn('total_animais');
            });
        }
    }
}
