<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableLotesDropValorArrobaColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('lotes', 'valor_arroba')) {
            Schema::table('lotes', function (Blueprint $table) {
                $table->dropColumn('valor_arroba');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasColumn('lotes', 'valor_arroba')) {
            Schema::table('lotes', function (Blueprint $table) {
                $table->float('valor_arroba')->nullable();
            });
        }
    }
}
