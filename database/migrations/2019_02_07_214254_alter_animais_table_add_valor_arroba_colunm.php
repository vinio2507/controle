<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAnimaisTableAddValorArrobaColunm extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('animais', 'valor_arroba')) {
            Schema::table('animais', function (Blueprint $table) {
                $table->float('valor_arroba')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('animais', 'valor_arroba')) {
            Schema::table('animais', function (Blueprint $table) {
                $table->dropColumn('valor_arroba');
            });
        }
    }
}
