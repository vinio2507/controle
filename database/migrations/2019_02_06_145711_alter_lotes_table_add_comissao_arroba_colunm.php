<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLotesTableAddComissaoArrobaColunm extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('lotes', 'comissao_arroba')) {
            Schema::table('lotes', function (Blueprint $table) {
                $table->float('comissao_arroba')->nullable();
                $table->unsignedInteger('tipo_frete_id')->nullable();
                $table->foreign('tipo_frete_id', 'tipo_frete_id_foreign')
                    ->references('id')->on('tipo_frete');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('lotes', 'comissao_arroba')) {
            Schema::table('lotes', function (Blueprint $table) {
                $table->dropForeign('tipo_frete_id_foreign');
                $table->dropColumn('tipo_frete');
                $table->dropColumn('comissao_arroba');
            });
        }
    }
}
