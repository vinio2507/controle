<?php

namespace App\Http\Controllers;

use App\Animal;
use App\Controle;
use App\FaixaFrete;
use App\Frete;
use App\TipoAnimal;
use App\TipoFrete;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ControleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tipos_animais = TipoAnimal::all();
        $tipos_frete = TipoFrete::all();
        $faixas = FaixaFrete::all();

        $numero = DB::table('lotes')->whereDate('created_at', date('Y-m-d'))->orderBy('created_at', 'desc')->first();

        if ($numero) {
            $numero = intval($numero->numero) + 1;
        } else {
            $numero = 1;
        }

        return view('index', [
            'tipos_animais' => $tipos_animais,
            'fretes' => $tipos_frete,
            'faixas' => $faixas,
            'numero_lote' => $numero]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $proprietario = $request->input('proprietario');
        $numero = $request->input('numero');
        $comissao_animal = parseFloat($request->input('comissao_animal'));
        $total_animais = $request->input('total_animais');
        $valor_total_frete = floatval($request->input('valor_total_frete'));
        $funrural = parseFloat($request->input('funrural'));

        if ($request->input('animal_quantidade')) {
            $animais = [];
            foreach ($request->input('animal_quantidade') as $key => $qtd) {
                $animal = [
                    'quantidade' => $qtd,
                    'tipo' => intval($request->input('animal_tipo')[$key]),
                    'peso' => parseFloat($request->input('animal_peso')[$key]),
                    'valor_arroba' => parseFloat($request->input('animal_valor_arroba')[$key]),
                ];
                $animais[] = $animal;
            }
        }

        if ($request->input('frete_distancia')) {
            $fretes = [];
            foreach ($request->input('frete_distancia') as $key => $distancia) {
                $frete = [
                    'distancia' => $distancia,
                    'tipo' => $request->input('frete_tipo')[$key],
                    'animais' => $request->input('frete_qtd_animais')[$key],
                    'valor' => floatval($request->input('frete_valor')[$key]),
                ];

                $fretes[] = $frete;
            }
        }


        $lote = new Controle();
        $lote->proprietario = $proprietario;
        $lote->numero = $numero;
        $lote->comissao = $comissao_animal;
        $lote->valor_frete = $valor_total_frete;
        $lote->total_animais = $total_animais;
        $lote->funrural = $funrural;
        $lote->save();


        foreach ($animais as $item) {
            $animal = new Animal();
            $animal->tipo_animal_id = $item['tipo'];
            $animal->lotes_id = $lote->id;
            $animal->quantidade = $item['quantidade'];
            $animal->peso_total = $item['peso'];
            $animal->valor_arroba = $item['valor_arroba'];
            $animal->save();
        }

        foreach ($fretes as $item) {
            $frete = new Frete();
            $frete->lotes_id = $lote->id;
            $frete->distancia = $item['distancia'];
            $frete->tipo_id = $item['tipo'];
            $frete->animais = $item['animais'];
            $frete->valor = $item['valor'];
            $frete->save();
        }

        return $this->index()->with('msg', 'Cadastrado com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Controle $controle
     * @return \Illuminate\Http\Response
     */
    public
    function show(Controle $controle)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Controle $controle
     * @return \Illuminate\Http\Response
     */
    public
    function edit(Controle $controle)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Controle $controle
     * @return \Illuminate\Http\Response
     */
    public
    function update(Request $request, Controle $controle)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Controle $controle
     * @return \Illuminate\Http\Response
     */
    public
    function destroy(Controle $controle)
    {
        //
    }
}
