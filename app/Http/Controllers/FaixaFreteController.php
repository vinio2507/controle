<?php

namespace App\Http\Controllers;

use App\FaixaFrete;
use App\TipoFrete;
use Illuminate\Http\Request;

class FaixaFreteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tipos = TipoFrete::all();
        $fretes = FaixaFrete::all();
        return view('faixafrete.index', ['fretes' => $fretes, 'tipos' => $tipos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->index();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tipo = intval($request->input('tipo'));
        $descricao = $request->input('descricao');
        $faixa_1 = floatval($request->input('faixa_1'));
        $faixa_2 = floatval($request->input('faixa_2'));
        $valor = floatval($request->input('valor'));

        $frete = new FaixaFrete();
        $frete->tipo = $tipo;
        $frete->descricao = $descricao;
        $frete->faixa_1 = $faixa_1;
        $frete->faixa_2 = $faixa_2;
        $frete->valor = $valor;
        $frete->save();

        $tipos = TipoFrete::all();
        $fretes = FaixaFrete::all();
        return view('faixafrete.index', ['fretes' => $fretes, 'tipos' => $tipos])->with('msg', 'Cadastrado com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TipoFrete $faixaFrete
     * @return \Illuminate\Http\Response
     */
    public function show(TipoFrete $faixaFrete)
    {
        $fretes = TipoFrete::all();
        return view('faixafrete.index', ['tipos' => $fretes, 'tipo' => $faixaFrete]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TipoFrete $faixaFrete
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $faixaFrete = FaixaFrete::find($id);
        $tipos = TipoFrete::all();
        $fretes = FaixaFrete::all();
        return view('faixafrete.index', ['tipos' => $tipos, 'frete' => $faixaFrete, 'fretes' => $fretes]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->input('tipo') != '')
            $tipo = $request->input('tipo');
        if ($request->input('descricao') != '')
            $descricao = $request->input('descricao');
        if ($request->input('faixa_1') != '')
            $faixa_1 = floatval($request->input('faixa_1'));
        if ($request->input('faixa_2') != '')
            $faixa_2 = floatval($request->input('faixa_2'));
        if ($request->input('valor') != '')
            $valor = floatval($request->input('valor'));


        $frete = FaixaFrete::find($id);
        if ($frete) {
            if ($request->input('tipo') != '')
                $frete->tipo = $tipo;
            if ($request->input('descricao') != '')
                $frete->descricao = $descricao;
            if ($request->input('faixa_1') != '')
                $frete->faixa_1 = $faixa_1;
            if ($request->input('faixa_2') != '')
                $frete->faixa_2 = $faixa_2;
            if ($request->input('valor') != '')
                $frete->valor = $valor;
            $frete->save();
        } else {
            return $this->index()->with('error', 'Faixa de frete não encontrada!');
        }

        return $this->index()->with('msg', 'Atualizado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public
    function destroy($id)
    {
        $frete = FaixaFrete::find($id);
        if ($frete) {
            $frete->delete();
        }else{
            return $this->index()->with('error', 'Faixa de frete não encontrada!');
        }
        return $this->index()->with('msg', 'Removido com sucesso!');
    }
}
