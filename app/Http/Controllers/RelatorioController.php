<?php

namespace App\Http\Controllers;

use App\Animal;
use App\Controle;
use App\Frete;
use App\TipoAnimal;
use App\TipoFrete;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PhpParser\PrettyPrinter\Standard;

class RelatorioController extends Controller
{
    public function geral()
    {
        return view('relatorios.geral');
    }

    public function imprimir(Request $request)
    {
        $request->request->add(['viewer', 'relatorios.imprimir']);
        return $this->dia($request);
    }

    public function dia(Request $request)
    {
        $viewer = $request->input('viewer');
        $viewer = $viewer ? $viewer : 'relatorios.dia';

        $data = $request->input('data_relatorio');

        $data_inicial = date('Y-m-d') . ' 00:00:00';
        $data_final = date('Y-m-d') . ' 23:59:59';

        if ($data) {
            $data_inicial = $data . ' 00:00:00';
            $data_final = $data . ' 23:59:59';
        }

        $lotes = DB::table('lotes')->whereBetween('created_at', [$data_inicial, $data_final])->get();
        $geral = new \stdClass();
        $geral->quantidade = 0;
        $geral->funrural = 0;

        $geral->valor = 0;
        $geral->valor_arroba = 0;
        $geral->valor_animal = 0;

        $geral->peso = 0;
        $geral->peso_arroba = 0;
        $geral->peso_medio_animal = 0;
        $geral->peso_medio_animal_arroba = 0;

        $geral->frete = 0;
        $geral->frete_arroba = 0;
        $geral->frete_animal = 0;

        $geral->comissao = 0;
        $geral->comissao_arroba = 0;
        $geral->comissao_animal = 0;

        $geral->custo = 0;
        $geral->custo_arroba = 0;
        $geral->custo_animal = 0;
        $tipos = [];

        if (count($lotes) > 0):
            foreach ($lotes as $key => $lote) {
                $lote->valor_total = 0;
                $lote->peso_total = 0;
                $lote->quantidade_animais = 0;
                $lote->valor_total_comissao = 0;
                $lote->tipos = [];
                foreach (TipoAnimal::all() as $tipo_id => $tipo) {
                    $lote->tipos[$tipo->id]['animais'] = [];
                    $lote->tipos[$tipo->id]['animais'] = Animal::where([['tipo_animal_id', '=', $tipo->id], ['lotes_id', '=', $lote->id]])->get()->toArray();
                    $lote->tipos[$tipo->id]['label'] = $tipo->descricao;
                    if (!isset($lote->tipos[$tipo->id]['valor']))
                        $lote->tipos[$tipo->id]['valor'] = 0;

                    if (!isset($lote->tipos[$tipo->id]['valor_total_comissao']))
                        $lote->tipos[$tipo->id]['valor_total_comissao'] = 0;

                    if (!isset($lote->tipos[$tipo->id]['quantidade']))
                        $lote->tipos[$tipo->id]['quantidade'] = 0;

                    if (!isset($lote->tipos[$tipo->id]['peso_total']))
                        $lote->tipos[$tipo->id]['peso_total'] = 0;

                    foreach ($lote->tipos[$tipo->id]['animais'] as $animal) {
                        $lote->tipos[$tipo->id]['valor'] += floatval(($animal['peso_total'] / 15) * $animal['valor_arroba']);
                        $lote->tipos[$tipo->id]['valor_total_comissao'] += floatval($animal['quantidade']) * ($animal['valor_arroba'] * ($lote->comissao / 100));
                        $lote->tipos[$tipo->id]['quantidade'] += intval($animal['quantidade']);
                        $lote->tipos[$tipo->id]['peso_total'] += floatval($animal['peso_total']);
                    }

                    if ($lote->tipos[$tipo->id]['quantidade'] > 0):
                        $frete_qtd = 0;

                        foreach (Frete::all() as $frete) {
                            if ($frete->lotes_id === $lote->id) {
                                $frete_qtd += $frete->animais;
                            }
                        }

                        $lote->tipos[$tipo->id]['frete'] = ((($lote->tipos[$tipo->id]['quantidade'] * 100) / $frete_qtd) / 100) * $lote->valor_frete;
                        $lote->tipos[$tipo->id]['frete_animal'] = $lote->tipos[$tipo->id]['frete'] / $lote->tipos[$tipo->id]['quantidade'];
                        $lote->tipos[$tipo->id]['frete_arroba'] = $lote->tipos[$tipo->id]['frete'] / ($lote->tipos[$tipo->id]['peso_total'] / 15);

                        $lote->tipos[$tipo->id]['comissao_animal'] = $lote->tipos[$tipo->id]['valor_total_comissao'] / $lote->tipos[$tipo->id]['quantidade'];
                        $lote->tipos[$tipo->id]['valor_total_comissao'] = $lote->tipos[$tipo->id]['comissao_animal'] * $lote->tipos[$tipo->id]['quantidade'];
                        $lote->tipos[$tipo->id]['comissao_arroba'] = $lote->tipos[$tipo->id]['valor_total_comissao'] / ($lote->tipos[$tipo->id]['peso_total'] / 15);

                        $lote->tipos[$tipo->id]['custo'] = $lote->tipos[$tipo->id]['frete'] + $lote->tipos[$tipo->id]['valor_total_comissao'] + $lote->tipos[$tipo->id]['valor'];
                        $lote->tipos[$tipo->id]['custo_animal'] = $lote->tipos[$tipo->id]['custo'] / $lote->tipos[$tipo->id]['quantidade'];
                        $lote->tipos[$tipo->id]['custo_arroba'] = $lote->tipos[$tipo->id]['custo'] / ($lote->tipos[$tipo->id]['peso_total'] / 15);

                        //Desconto pelo tipo de animal
                        if ($tipo->desconto > 0) {
                            $lote->tipos[$tipo->id]['valor'] = $lote->tipos[$tipo->id]['valor'] - (($lote->tipos[$tipo->id]['valor'] * ($tipo->desconto / 100)));
                        }
                        $lote->tipos[$tipo->id]['peso'] = Animal::where([['tipo_animal_id', '=', $tipo->id], ['lotes_id', '=', $lote->id]])->sum('peso_total');
                        $lote->valor_total += $lote->tipos[$tipo->id]['valor'];
                        $lote->valor_total_comissao += $lote->tipos[$tipo->id]['valor_total_comissao'];
                        $lote->peso_total += $lote->tipos[$tipo->id]['peso'];
                        $lote->quantidade_animais += $lote->tipos[$tipo->id]['quantidade'];
                        $lote->tipos[$tipo->id]['valor_animal'] = $lote->tipos[$tipo->id]['valor'] / $lote->tipos[$tipo->id]['quantidade'];
                        $lote->tipos[$tipo->id]['valor_arroba'] = $lote->tipos[$tipo->id]['valor'] / ($lote->tipos[$tipo->id]['peso_total'] / 15);
                        $tipos[$tipo->id]['label'] = $tipo->descricao;

                        if (!isset($tipos[$tipo->id]['valor'])) $tipos[$tipo->id]['valor'] = 0;
                        $tipos[$tipo->id]['valor'] += $lote->tipos[$tipo->id]['valor'];

                        if (!isset($tipos[$tipo->id]['quantidade'])) $tipos[$tipo->id]['quantidade'] = 0;
                        $tipos[$tipo->id]['quantidade'] += $lote->tipos[$tipo->id]['quantidade'];

                        if (!isset($tipos[$tipo->id]['peso_total'])) $tipos[$tipo->id]['peso_total'] = 0;
                        $tipos[$tipo->id]['peso_total'] += $lote->tipos[$tipo->id]['peso_total'];
                        $tipos[$tipo->id]['valor_animal'] = $tipos[$tipo->id]['valor'] / $tipos[$tipo->id]['quantidade'];
                        $tipos[$tipo->id]['valor_arroba'] = $tipos[$tipo->id]['valor'] / ($tipos[$tipo->id]['peso_total'] / 15);

                        if (!isset($tipos[$tipo->id]['frete'])) $tipos[$tipo->id]['frete'] = 0;
                        $tipos[$tipo->id]['frete'] += $lote->tipos[$tipo->id]['frete'];
                        $tipos[$tipo->id]['frete_animal'] = $tipos[$tipo->id]['frete'] / $tipos[$tipo->id]['quantidade'];
                        $tipos[$tipo->id]['frete_arroba'] = $tipos[$tipo->id]['frete'] / ($tipos[$tipo->id]['peso_total'] / 15);

                        if (!isset($tipos[$tipo->id]['valor_total_comissao'])) $tipos[$tipo->id]['valor_total_comissao'] = 0;
                        $tipos[$tipo->id]['valor_total_comissao'] += $lote->tipos[$tipo->id]['valor_total_comissao'];
                        $tipos[$tipo->id]['comissao_animal'] = $tipos[$tipo->id]['valor_total_comissao'] / $tipos[$tipo->id]['quantidade'];
                        $tipos[$tipo->id]['comissao_arroba'] = $tipos[$tipo->id]['valor_total_comissao'] / ($tipos[$tipo->id]['peso_total'] / 15);

                        if (!isset($tipos[$tipo->id]['custo'])) $tipos[$tipo->id]['custo'] = 0;
                        $tipos[$tipo->id]['custo'] += $lote->tipos[$tipo->id]['custo'];
                        $tipos[$tipo->id]['custo_animal'] = $tipos[$tipo->id]['custo'] / $tipos[$tipo->id]['quantidade'];
                        $tipos[$tipo->id]['custo_arroba'] = $tipos[$tipo->id]['custo'] / ($tipos[$tipo->id]['peso_total'] / 15);

                    endif;
                }

                $geral->quantidade += $lote->quantidade_animais;
                $geral->valor += $lote->valor_total;
                $geral->peso += $lote->peso_total;
                $geral->frete += $lote->valor_frete;
                $geral->comissao += $lote->valor_total_comissao;

                $lote->animais_tipos = $tipos;
                $lote->peso_medio = $lote->peso_total / $lote->quantidade_animais;
                $lote->peso_arroba = $lote->peso_total / 15;
                $lote->comissao_total = $lote->valor_total_comissao;
                $lote->frete_arroba = $lote->valor_frete / $lote->peso_arroba;
                $lote->frete_animal = $lote->valor_frete / $lote->quantidade_animais;
                $lote->valor_animal = $lote->valor_total / $lote->quantidade_animais;
                $lote->valor_arroba = $lote->valor_total / $lote->peso_arroba;
                $lote->custo_animal = ($lote->valor_total + $lote->valor_frete + $lote->valor_total_comissao) / $lote->quantidade_animais;
                $lote->custo_arroba = ($lote->valor_total + $lote->valor_frete + $lote->valor_total_comissao) / ($lote->peso_total / 15);
                $lote->comissao_animal = $lote->valor_total_comissao / $lote->quantidade_animais;
                $lote->comissao_arroba = $lote->valor_total_comissao / $lote->peso_arroba;
                $lote->custo = $lote->valor_total + $lote->valor_total_comissao + $lote->valor_frete;

                //Calculo do Funrural
                if ($lote->funrural != null && $lote->funrural > 0)
                    $lote->custo += ($lote->funrural / 100) * $lote->valor_total;
                $geral->custo += $lote->custo;
                $lote->custo_arroba = $lote->custo / $lote->peso_arroba;
                $lote->custo_animal = $lote->custo / $lote->quantidade_animais;
            }


            $geral->peso_arroba = $geral->peso / 15;
            $geral->peso_medio_animal = $geral->peso / $geral->quantidade;
            $geral->peso_medio_animal_arroba = $geral->peso_medio_animal / 15;

            $geral->valor_arroba = $geral->valor / $geral->peso_arroba;
            $geral->valor_animal = $geral->valor / $geral->quantidade;

            $geral->frete_arroba = $geral->frete / $geral->peso_arroba;
            $geral->frete_animal = $geral->frete / $geral->quantidade;

            $geral->comissao_arroba = $geral->comissao / $geral->peso_arroba;
            $geral->comissao_animal = $geral->comissao / $geral->quantidade;

            $geral->custo_arroba = $geral->custo / $geral->peso_arroba;
            $geral->custo_animal = $geral->custo / $geral->quantidade;


            $dados = [
                [
                    'label' => 'Valor Total',
                    'value' => 'R$' . number_format($geral->valor, 2, ',', '.'),
                ], [
                    'label' => 'Valor por Animal',
                    'value' => 'R$' . number_format($geral->valor_animal, 2, ',', '.'),
                ], [
                    'label' => 'Valor por Arroba',
                    'value' => 'R$' . number_format($geral->valor_arroba, 2, ',', '.'),
                ], [
                    'label' => 'Total de Animais',
                    'value' => $geral->quantidade,
                ], [
                    'label' => 'Total de Frete',
                    'value' => 'R$' . number_format($geral->frete, 2, ',', '.'),
                ], [
                    'label' => 'Frete por Animal',
                    'value' => 'R$' . number_format($geral->frete_animal, 2, ',', '.'),
                ], [
                    'label' => 'Frete por Arroba',
                    'value' => 'R$' . number_format($geral->frete_arroba, 2, ',', '.'),
                ], [
                    'label' => 'Peso Total',
                    'value' => number_format($geral->peso, 2, ',', '.') . 'KG',
                    'leg' => number_format($geral->peso_arroba, 2, ',', '.') . '@',
                ], [
                    'label' => 'Comissão Total',
                    'value' => 'R$' . number_format($geral->comissao, 2, ',', '.'),
                ], [
                    'label' => 'Comissão Animal',
                    'value' => 'R$' . number_format($geral->comissao_animal, 2, ',', '.'),
                ], [
                    'label' => 'Comissão Arroba',
                    'value' => 'R$' . number_format($geral->comissao_arroba, 2, ',', '.'),
                ], [
                    'label' => 'Peso Médio',
                    'value' => number_format($geral->peso_medio_animal, 2, ',', '.') . 'KG',
                    'leg' => number_format($geral->peso_medio_animal_arroba, 2, ',', '.') . '@',
                ], [
                    'label' => 'Custo Total',
                    'value' => 'R$' . number_format($geral->custo, 2, ',', '.'),
                ], [
                    'label' => 'Custo por Arroba',
                    'value' => 'R$' . number_format($geral->custo_arroba, 2, ',', '.'),
                ], [
                    'label' => 'Custo por Animal',
                    'value' => 'R$' . number_format($geral->custo_animal, 2, ',', '.'),
                ]
            ];

            foreach ($tipos as $key => $tipo) {
                $valor = $tipo['valor'];
                $quantidade = $tipo['quantidade'];

                if ($quantidade > 0) {
                    $peso_arroba = $tipo['peso_total'] / 15;
                    $valor_arroba = $valor / $peso_arroba;
                    $valor_animal = $valor / $quantidade;


                    $dados[] = [
                        'label' => '#!#section#!#',
                        'value' => 'Informações dos animais do tipo ' . $tipo['label']
                    ];

                    $dados[] = [
                        'label' => '#!#inicio#!#',
                    ];

                    $dados[] = [
                        'label' => 'Peso total dos animais do tipo ' . $tipo['label'],
                        'value' => number_format($tipo['peso_total'], 2, '.', ',') . 'KG'
                    ];

                    $dados[] = [
                        'label' => 'Quantidade de animais do tipo ' . $tipo['label'],
                        'value' => $quantidade
                    ];

                    $dados[] = [
                        'label' => '#!#inicio#!#',
                    ];

                    $dados[] = [
                        'label' => 'Valor Total Animais do tipo ' . $tipo['label'],
                        'value' => 'R$' . number_format($tipo['valor'], 2, ',', '.'),
                    ];

                    $dados[] = [
                        'label' => 'Valor por animal do tipo ' . $tipo['label'],
                        'value' => 'R$' . number_format($valor_animal, 2, ',', '.'),
                    ];

                    $dados[] = [
                        'label' => 'Valor por Arroba dos Animais do tipo ' . $tipo['label'],
                        'value' => 'R$' . number_format($valor_arroba, 2, ',', '.'),
                    ];


                    $dados[] = [
                        'label' => '#!#inicio#!#',
                    ];


                    $dados[] = [
                        'label' => 'Frete Total Animais do tipo ' . $tipo['label'],
                        'value' => 'R$' . number_format($tipo['frete'], 2, ',', '.'),
                    ];

                    $dados[] = [
                        'label' => 'Frete por animal do tipo ' . $tipo['label'],
                        'value' => 'R$' . number_format($tipo['frete_animal'], 2, ',', '.'),
                    ];

                    $dados[] = [
                        'label' => 'Frete por arroba dos animais do tipo ' . $tipo['label'],
                        'value' => 'R$' . number_format($tipo['frete_arroba'], 2, ',', '.'),
                    ];


                    $dados[] = [
                        'label' => '#!#inicio#!#',
                    ];


                    $dados[] = [
                        'label' => 'Comissão total animais do tipo ' . $tipo['label'],
                        'value' => 'R$' . number_format($tipo['valor_total_comissao'], 2, ',', '.'),
                    ];

                    $dados[] = [
                        'label' => 'Comissão por animal do tipo ' . $tipo['label'],
                        'value' => 'R$' . number_format($tipo['comissao_animal'], 2, ',', '.'),
                    ];

                    $dados[] = [
                        'label' => 'Comissão por arroba dos animais do tipo ' . $tipo['label'],
                        'value' => 'R$' . number_format($tipo['comissao_arroba'], 2, ',', '.'),
                    ];


                    $dados[] = [
                        'label' => '#!#inicio#!#',
                    ];


                    $dados[] = [
                        'label' => 'Custo total animais do tipo ' . $tipo['label'],
                        'value' => 'R$' . number_format($tipo['custo'], 2, ',', '.'),
                    ];

                    $dados[] = [
                        'label' => 'Custo por animal do tipo ' . $tipo['label'],
                        'value' => 'R$' . number_format($tipo['custo_animal'], 2, ',', '.'),
                    ];

                    $dados[] = [
                        'label' => 'Custo por arroba dos animais do tipo ' . $tipo['label'],
                        'value' => 'R$' . number_format($tipo['custo_arroba'], 2, ',', '.'),
                    ];


                }
            }
        else:
            $dados = [];
        endif;

        return view($viewer, [
            'geral' => $geral,
            'tipos' => $tipos,
            'dados' => $dados,
            'data' => $data,
            'lotes' => $lotes]);
    }
}
