<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TipoFrete;

class TipoFreteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $tipos = TipoFrete::all();
        return view('tipofrete.index', ['tipos' => $tipos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->index();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $descricao = $request->input('descricao');
        $quantidade = intval($request->input('quantidade'));

        $tipo = new TipoFrete();
        $tipo->descricao = $descricao;
        $tipo->quantidade_animais = $quantidade;
        $tipo->save();

        $tipos = TipoFrete::all();
        return view('tipofrete.index', ['tipos' => $tipos])->with('msg', 'Cadastrado com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TipoFrete $tipoFrete
     * @return \Illuminate\Http\Response
     */
    public function show(TipoFrete $tipoFrete)
    {
        return $this->index();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TipoFrete $tipoFrete
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tipoFrete = TipoFrete::find($id);
        $tipos = TipoFrete::all();
        return view('tipofrete.index', ['tipos' => $tipos, 'tipo' => $tipoFrete]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->input('descricao') != '')
            $descricao = $request->input('descricao');
        if ($request->input('quantidade') != '')
            $quantidade = intval($request->input('quantidade'));

        $tipo = TipoFrete::find($id);
        if ($request->input('descricao') != '')
            $tipo->descricao = $descricao;
        if ($request->input('quantidade') != '')
            $tipo->quantidade_animais = $quantidade;
        $tipo->save();

        return $this->index()->with('msg', 'Atualizado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tipoFrete = TipoFrete::find($id);
        $tipoFrete->delete();

        return $this->index()->with('msg', 'Removido com sucesso!');
    }
}
