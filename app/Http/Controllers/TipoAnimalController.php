<?php

namespace App\Http\Controllers;

use App\TipoAnimal;
use Illuminate\Http\Request;

class TipoAnimalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tipos = TipoAnimal::all();
        return view('tipoanimal.index', ['tipos' => $tipos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tipos = TipoAnimal::all();
        return view('tipoanimal.index', ['tipos' => $tipos]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $descricao = $request->input('descricao');
        $faixa_1 = floatval($request->input('peso_inicial'));
        $faixa_2 = floatval($request->input('peso_final'));
        $desconto = floatval($request->input('desconto'));

        $tipo = new TipoAnimal();
        $tipo->descricao = $descricao;
        $tipo->faixa_1 = $faixa_1;
        $tipo->faixa_2 = $faixa_2;
        $tipo->desconto = $desconto;
        $tipo->save();

        return $this->index()->with('msg', 'Cadastrado com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TipoAnimal $tipoAnimal
     * @return \Illuminate\Http\Response
     */
    public function show(TipoAnimal $tipoAnimal)
    {
        $tipos = TipoAnimal::all();
        return view('tipoanimal.index', ['tipos' => $tipos, 'tipo' => $tipoAnimal]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TipoAnimal $tipoAnimal
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tipoAnimal = TipoAnimal::find($id);
        return $this->index()->with('tipo', $tipoAnimal);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\TipoAnimal $tipoAnimal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->input('descricao') != '')
            $descricao = $request->input('descricao');
        if ($request->input('peso_inicial') != '')
            $faixa_1 = floatval($request->input('peso_inicial'));
        if ($request->input('peso_final') != '')
            $faixa_2 = floatval($request->input('peso_final'));
        if ($request->input('desconto') != '')
            $desconto = floatval($request->input('desconto'));

        $tipo = TipoAnimal::find($id);
        if ($request->input('descricao') != '')
            $tipo->descricao = $descricao;
        if ($request->input('peso_inicial') != '')
            $tipo->faixa_1 = $faixa_1;
        if ($request->input('peso_final') != '')
            $tipo->faixa_2 = $faixa_2;
        if ($request->input('desconto') != '')
            $tipo->desconto = $desconto;
        $tipo->save();

        $tipos = TipoAnimal::all();
        return view('tipoanimal.index', ['tipos' => $tipos])->with('msg', 'Atualizado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TipoAnimal $tipoAnimal
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tipoAnimal = TipoAnimal::find($id);
        $tipoAnimal->delete();
        $tipos = TipoAnimal::all();
        return view('tipoanimal.index', ['tipos' => $tipos])->with('msg', 'Removido com sucesso!');
    }
}
