<?php
function parseFloat($value)
{
    if (gettype($value) == 'string') {
        $floate = floatval(str_replace('.', '', $value));
        if (strpos($value, ',') && strpos($value, ',')) {
            $floate = str_replace('.', '', $value);
            return floatval(str_replace(',', '.', $floate));
        } else if (strpos($value, ',')) {
            $floate = floatval(str_replace(',', '.', $value));
            return $floate >= floatval($value) + 1 ? floatval($value) : $floate;
        }
        return $floate;
    } else {
        return floatval($value);
    }
}
