<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoFrete extends Model
{
    protected $table = 'tipo_frete';
}
