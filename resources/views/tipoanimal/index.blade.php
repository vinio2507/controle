@extends('template')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <h2>
                        Tipos de animais <a class="btn btn-outline-primary btn-sm"
                                            href="{{route('tipoanimal.create')}}">Adicionar Novo</a>
                    </h2>
                    <hr>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <form method="post"
                      action="{{isset($tipo)?route('tipoanimal.update', ['id' => $tipo->id]):route('tipoanimal.store')}}">
                    @if(isset($tipo)) <input type="hidden" name="_method" value="PUT"> @endif
                        {{ csrf_field() }}

                    <div class="form-group">
                        <label for="descricao">Descrição</label>
                        <input value="{{isset($tipo)?$tipo->descricao:''}}" id="descricao" name="descricao" type="text"
                               class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="peso_inicial">Peso Inicial</label>
                        <input value="{{isset($tipo)?$tipo->faixa_1:''}}" step="any" id="peso_inicial" name="peso_inicial"
                               type="number" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="peso_final">Peso Final</label>
                        <input value="{{isset($tipo)?$tipo->faixa_2:''}}" step="any" id="peso_final" name="peso_final"
                               type="number" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="desconto">Desconto (%)</label>
                        <input value="{{isset($tipo)?$tipo->desconto:''}}" step="any" id="desconto" name="desconto" type="number"
                               class="form-control">
                    </div>
                    <button type="submit" class="btn btn-success">{{isset($tipo)?'Atualizar':'Cadastrar'}}</button>
                </form>
            </div>
            <div class="col-md-8">
                <table class="table table-striped">
                    <thead>
                    <th>Descricao</th>
                    <th>Peso Inicial</th>
                    <th>Peso Final</th>
                    <th>Desconto</th>
                    <th>Ações</th>
                    </thead>
                    <tbody>
                    @forelse($tipos as $tipo)
                        <tr>
                            <td>{{$tipo->descricao}}</td>
                            <td>{{$tipo->faixa_1}}</td>
                            @if($tipo->faixa_2 == 0)
                                <td class="text-center"> -</td>
                            @else
                                <td>{{$tipo->faixa_2}}</td>
                            @endif
                            @if($tipo->desconto == 0)
                                <td class="text-center"> -</td>
                            @else
                                <td>{{$tipo->desconto}}%</td>
                            @endif
                            <td><a href="{{route('tipoanimal.edit', $tipo)}}"
                                   class="btn btn-sm btn-primary">Editar</a>
                                <form style="display: inline-block;" method="POST" action="{{route('tipoanimal.destroy', $tipo->id)}}">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button
                                   class="btn btn-sm btn-danger">Excluir</button></form></td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5"><strong>Nenhum item encontrado!</strong></td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
