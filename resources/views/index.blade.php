@extends('template')
@section('content')
    <script>
        const faixas = @json($faixas);
        const tipos_frete = @json($fretes);
    </script>
    <div class="container">
        <form method="post" action="{{route('controle.store')}}">
            {{csrf_field()}}
            <div class="row form-container pb-3">
                <div class="col-md-12">
                    <div class="section-header">
                        <h3>Informações do Lote
                            <button class="btn btn-success float-right">Cadastrar</button>
                        </h3>
                        <hr>
                    </div>
                    <div class="form-group">
                        <label for="proprietario">Proprietário</label>
                        <input id="proprietario" name="proprietario" type="text" class="form-control">
                    </div>
                    <div class="row pb-5">
                        <div class="col-md-4">
                            <label for="numero"><span class="required">*</span> Número do lote</label>
                            <input id="numero" required name="numero" value="{{$numero_lote}}" type="number" step="any"
                                   class="form-control">
                        </div>
                        <div class="col-md-4">
                            <label for="comissao_animal">Comissão por animal (% referente ao valor da arroba)</label>
                            <input value="10" id="comissao_animal" name="comissao_animal" type="number" step="any"
                                   class="form-control">
                        </div>
                        <div class="col-md-4">
                            <label for="comissao_animal">Funrual (%)</label>
                            <input value="1.5" id="funrural" name="funrural" type="number" step="any"
                                   class="form-control">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="section-header">
                        <h3>Informações dos Animais</h3>
                        <hr>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="quantidade_animal">Quantidade</label>
                                <input id="quantidade_animal" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3 p-md-0">
                            <div class="form-group">
                                <label for="tipo_animal">Tipo</label>
                                <select id="tipo_animal" class="form-control">
                                    <option value="">Tipo</option>
                                    @foreach($tipos_animais as $animal)
                                        <option value="{{$animal->id}}">{{$animal->descricao}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3 pr-md-0">
                            <div class="form-group">
                                <label for="peso_total">Peso</label>
                                <input id="peso_total" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label style="font-size: 12px; white-space: nowrap" for="valor_arroba">Valor da
                                    Arroba</label>
                                <input id="valor_arroba" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <a id="adicionar_animal" class="btn btn-primary btn-sm btn-block">Adicionar</a>
                        </div>
                        <div class="col-md-12 mt-3 tabela-animais table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Qtd</th>
                                    <th>Tipo</th>
                                    <th>Peso</th>
                                    <th>Valor da Arroba</th>
                                    <th>Ações</th>
                                </tr>
                                <tbody>

                                </tbody>
                                </thead>
                            </table>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group d-inline-block">
                                <label for="">Total de Animais</label>
                                <input id="total_animais" name="total_animais" type="hidden" step="any"
                                       value="0" class="form-control">
                                <input id="total_animais_disabled" disabled type="number" step="any"
                                       value="0" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="section-header">
                        <h3>Informações do Frete</h3>
                        <hr>
                    </div>
                    <div class="row">

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="distancia">Distância (km)</label>
                                <input id="distancia" min="0" type="number"
                                       step="any"
                                       class="form-control">
                            </div>
                        </div>

                        <div class="col-md-6 p-md-0">
                            <div class="form-group">
                                <label for="tipo_frete">Tipo de Frete</label>
                                <select id="tipo_frete" class="form-control">
                                    <option value="">Selecione um tipo</option>
                                    @foreach($fretes as $frete)
                                        <option qtd="{{$frete->quantidade_animais}}"
                                                value="{{$frete->id}}">{{$frete->descricao}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="qtd_animais">Qtd Animais</label>
                                <input min="1" id="qtd_animais" type="number" step="any"
                                       class="form-control">
                            </div>
                        </div>

                        {{--<div class="col-md-3">--}}
                        {{--<div class="form-group">--}}
                        {{--<label for="valor_frete">Valor do Frete</label>--}}
                        {{--<input id="valor_frete" name="valor_frete" type="number" step="any"--}}
                        {{--class="form-control">--}}
                        {{--</div>--}}
                        {{--</div>--}}

                    </div>

                    <a id="adicionar_frete" class="btn btn-primary btn-block btn-sm">
                        Adicionar
                    </a>

                    <div class="row">
                        <div class="col-md-12 mt-3 tabela-fretes table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Distância</th>
                                    <th>Tipo</th>
                                    <th>Qtd de Animais</th>
                                    <th>Valor</th>
                                    <th>Ações</th>
                                </tr>
                                <tbody>

                                </tbody>
                                </thead>
                            </table>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group d-inline-block">
                                <label for="">Total do Frete</label>
                                <input id="valor_total_frete_disabled" disabled type="number" step="any"
                                       value="0.00" class="form-control">
                                <input id="valor_total_frete" type="hidden" name="valor_total_frete" step="any"
                                       value="0.00" class="form-control">
                            </div>
                            <div class="form-group d-inline-block">
                                <label for="">Total de animais</label>
                                <input id="total_animais_frete" type="number" step="any"
                                       value="0" class="form-control" disabled>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

