@extends('template')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <h2>
                        Tipos de Frete <a class="btn btn-outline-primary btn-sm"
                                            href="{{route('tipofrete.create')}}">Adicionar Novo</a>
                    </h2>
                    <hr>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <form method="post"
                      action="{{isset($tipo)?route('tipofrete.update', ['id' => $tipo->id]):route('tipofrete.store')}}">
                    @if(isset($tipo)) <input type="hidden" name="_method" value="PUT"> @endif
                        {{ csrf_field() }}

                    <div class="form-group">
                        <label for="descricao">Descrição</label>
                        <input value="{{isset($tipo)?$tipo->descricao:''}}" id="descricao" name="descricao" type="text"
                               class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="quantidade">Quantidade de Animais</label>
                        <input value="{{isset($tipo)?$tipo->quantidade_animais:''}}" id="quantidade" name="quantidade" type="text"
                               class="form-control">
                    </div>
                    <button type="submit" class="btn btn-success">{{isset($tipo)?'Atualizar':'Cadastrar'}}</button>
                </form>
            </div>
            <div class="col-md-8">
                <table class="table table-striped">
                    <thead>
                    <th>#</th>
                    <th>Descricao</th>
                    <th>Quantidade de Animais</th>
                    <th>Ações</th>
                    </thead>
                    <tbody>
                    @forelse($tipos as $tipo)
                        <tr>
                            <td>{{$tipo->id}}</td>
                            <td>{{$tipo->descricao}}</td>
                            <td>{{$tipo->quantidade_animais}}</td>
                            <td><a href="{{route('tipofrete.edit', $tipo)}}"
                                   class="btn btn-sm btn-primary">Editar</a>
                                <form style="display: inline-block;" method="POST" action="{{route('tipofrete.destroy', $tipo->id)}}">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <button
                                            class="btn btn-sm btn-danger">Excluir</button></form></td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="4"><strong>Nenhum item encontrado!</strong></td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
