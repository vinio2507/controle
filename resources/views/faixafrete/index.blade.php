@extends('template')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <h2>
                        Faixas de Frete <a class="btn btn-outline-primary btn-sm"
                                           href="{{route('faixafrete.create')}}">Adicionar Nova</a>
                    </h2>
                    <hr>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <form method="post"
                      action="{{isset($frete)?route('faixafrete.update', ['id' => $frete->id]):route('faixafrete.store')}}">
                    @if(isset($frete)) <input type="hidden" name="_method" value="PUT"> @endif
                        {{ csrf_field() }}

                    <div class="form-group">
                        <label for="tipo"><span class="required">*</span> Tipo de Frete</label>
                        <select required id="tipo" name="tipo" class="form-control">
                            <option value="">Selecione um Tipo</option>
                            @foreach($tipos as $tipo)
                                <option {{isset($frete) && $tipo->id = $frete->tipo?' selected ':''}} value="{{$tipo->id}}">{{$tipo->descricao}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="descricao">Descrição</label>
                        <input value="{{isset($frete)?$frete->descricao:''}}" id="descricao" name="descricao"
                               type="text"
                               class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="peso_inicial"><span class="required">*</span> Distância Inicial (km)</label>
                        <input required value="{{isset($frete)?$frete->faixa_1:''}}" step="any" id="faixa_1" name="faixa_1"
                               type="number" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="peso_final">Distância Final (km)</label>
                        <input value="{{isset($frete)?$frete->faixa_2:''}}" step="any" id="faixa_2" name="faixa_2"
                               type="number" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="valor"><span class="required">*</span> Valor</label>
                        <input required value="{{isset($frete)?$frete->valor:''}}" step="any" id="valor" name="valor"
                               type="number"
                               class="form-control">
                    </div>
                    <button type="submit" class="btn btn-success">{{isset($frete)?'Atualizar':'Cadastrar'}}</button>
                </form>
            </div>
            <div class="col-md-8">
                <table class="table table-striped">
                    <thead>
                    <th>Tipo</th>
                    <th>Descricao</th>
                    <th>Distância Inicial</th>
                    <th>Distância Final</th>
                    <th>Valor</th>
                    <th>Ações</th>
                    </thead>
                    <tbody>
                    @forelse($fretes as $frete)
                        <tr>
                            @foreach($tipos as $tipo)
                                @if($tipo->id == $frete->tipo)
                                    <td>{{$tipo->descricao}}</td>
                                @endif
                            @endforeach
                            <td>{{$frete->descricao}}</td>
                            <td>{{$frete->faixa_1}} km</td>
                            @if($frete->faixa_2 == 0)
                                <td class="text-center"> -</td>
                            @else
                                <td>{{$frete->faixa_2}} km</td>
                            @endif
                            @if($frete->valor == 0)
                                <td class="text-center"> -</td>
                            @else
                                <td>R$ {{$frete->valor}}</td>
                            @endif
                            <td><a href="{{route('faixafrete.edit', $frete)}}"
                                   class="btn btn-sm btn-primary">Editar</a>
                                <form style="display: inline-block;" method="POST"
                                      action="{{route('faixafrete.destroy', $frete->id)}}">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <button
                                            class="btn btn-sm btn-danger">Excluir
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5"><strong>Nenhum item encontrado!</strong></td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
