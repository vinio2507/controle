<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Imprimir Relatório</title>
</head>
<body>
<div class="content">
    <table border=1>
        <tr>
            <td>
                <header>
                    <h1 class="title">Relatório de compras referente
                        à {{$data?date('d/m/Y', strtotime($data)):date('d/m/Y')}}</h1>
                </header>
            </td>
        </tr>
        <tr class="lotes">
            <td>
                <table border=1>
                    <tr>
                        <th class="text-left">Valor Total</th>
                        <th class="text-left">Valor médio por arroba</th>
                        <th class="text-left">Valor Médio por Animal</th>
                        <th class="text-left">Custo Total</th>
                        <th class="text-left">Custo Médio por Arroba</th>
                        <th class="text-left">Custo Médio por Animal</th>
                    </tr>
                    <tr>
                        <td class="text-left other-value" style="white-space: nowrap;">
                            R$ {{number_format($geral->valor, 2, ',', '.')}}</td>
                        <td class="text-left other-value">R$ {{number_format($geral->valor_arroba, 2, ',', '.')}}</td>
                        <td class="text-left other-value">R$ {{number_format($geral->valor_animal, 2, ',', '.')}}</td>
                        <td class="text-left other-value">R$ {{number_format($geral->custo, 2, ',', '.')}}</td>
                        <td class="text-left other-value">R$ {{number_format($geral->custo_arroba, 2, ',', '.')}}</td>
                        <td class="text-left other-value">R$ {{number_format($geral->custo_animal, 2, ',', '.')}}</td>
                    </tr>
                    <tr>
                        <th class="text-left">Frete Total</th>
                        <th class="text-left">Frete por Arroba</th>
                        <th class="text-left">Frete por Animal</th>
                        <th class="text-left">Total de Comissão</th>
                        <th class="text-left">Comissão por Arroba</th>
                        <th class="text-left">Comissão por Animal</th>
                    </tr>
                    <tr>
                        <td class="text-left value">R$ {{number_format($geral->frete, 2, ',', '.')}}</td>
                        <td class="text-left value">R$ {{number_format($geral->frete_arroba, 2, ',', '.')}}</td>
                        <td class="text-left value">R$ {{number_format($geral->frete_animal, 2, ',', '.')}}</td>
                        <td class="text-left value">R$ {{number_format($geral->comissao, 2, ',', '.')}}</td>
                        <td class="text-left value">R$ {{number_format($geral->comissao_arroba, 2, ',', '.')}}</td>
                        <td class="text-left value">R$ {{number_format($geral->comissao_animal, 2, ',', '.')}}</td>
                    </tr>
                </table>
            </td>
        </tr>

        @foreach($tipos as $tipo)
            <tr>
                <td class="text-center"><strong>Animais do tipo {{$tipo['label']}}</strong></td>
            </tr>
            <tr class="lotes">

                <td>
                    <table border=1>
                        <tr>
                            <th class="text-left">Valor Total</th>
                            <th class="text-left">Valor médio por arroba</th>
                            <th class="text-left">Valor Médio por Animal</th>
                            <th class="text-left">Custo Total</th>
                            <th class="text-left">Custo Médio por Arroba</th>
                            <th class="text-left">Custo Médio por Animal</th>
                        </tr>
                        <tr>
                            <td class="text-left other-value" style="white-space: nowrap;">
                                R$ {{number_format($tipo['valor'], 2, ',', '.')}}</td>
                            <td class="text-left other-value">
                                R$ {{number_format($tipo['valor_arroba'], 2, ',', '.')}}</td>
                            <td class="text-left other-value">
                                R$ {{number_format($tipo['valor_animal'], 2, ',', '.')}}</td>
                            <td class="text-left other-value">R$ {{number_format($tipo['custo'], 2, ',', '.')}}</td>
                            <td class="text-left other-value">
                                R$ {{number_format($tipo['custo_arroba'], 2, ',', '.')}}</td>
                            <td class="text-left other-value">
                                R$ {{number_format($tipo['custo_animal'], 2, ',', '.')}}</td>
                        </tr>
                        <tr>
                            <th class="text-left">Frete Total</th>
                            <th class="text-left">Frete por Arroba</th>
                            <th class="text-left">Frete por Animal</th>
                            <th class="text-left">Total de Comissão</th>
                            <th class="text-left">Comissão por Arroba</th>
                            <th class="text-left">Comissão por Animal</th>
                        </tr>
                        <tr>
                            <td class="text-left value">R$ {{number_format($tipo['frete'], 2, ',', '.')}}</td>
                            <td class="text-left value">R$ {{number_format($tipo['frete_arroba'], 2, ',', '.')}}</td>
                            <td class="text-left value">R$ {{number_format($tipo['frete_animal'], 2, ',', '.')}}</td>
                            <td class="text-left value">R$ {{number_format($tipo['valor_total_comissao'], 2, ',', '.')}}</td>
                            <td class="text-left value">R$ {{number_format($tipo['comissao_arroba'], 2, ',', '.')}}</td>
                            <td class="text-left value">R$ {{number_format($tipo['comissao_animal'], 2, ',', '.')}}</td>
                        </tr>
                    </table>
                </td>
            </tr>
        @endforeach


        <tr>
            <td><h3 class="text-center">Relatório completo</h3></td>
        </tr>
        @foreach($lotes as $key => $lote)
            {{--@if($key == 9)--}}
            {{--<tr>--}}
            {{--<td class="quebrar-pagina"--}}
            {{--style="page-break-after: always; border: none; display: none; visibility: hidden;"></td>--}}
            {{--</tr>--}}
            {{--@endif--}}
            {{--@if($key % 11 == 0 && $key != 11 && $key != 0)--}}
            {{--<tr>--}}
            {{--<td class="quebrar-pagina"--}}
            {{--style="page-break-after: always; border: none; display: none; visibility: hidden;"></td>--}}
            {{--</tr>--}}
            {{--@endif--}}
            <tr class="lotes">
                <td>
                    <table border=1>
                        <thead>
                        <tr>
                            <td colspan="3"><strong>Proprietário: </strong>{{$lote->proprietario}}</td>
                            <td colspan="3"><strong>Número do lote: </strong>{{$lote->numero}}</td>
                        </tr>
                        <tr>
                            <th class="text-left">Valor Total</th>
                            <th class="text-left">Valor Arroba</th>
                            <th class="text-left">Valor Animal</th>
                            <th class="text-left">Custo Total</th>
                            <th class="text-left">Custo por Arroba</th>
                            <th class="text-left">Custo por Animal</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="text-left other-value">R$ {{number_format($lote->valor_total,2,',','.')}}</td>
                            <td class="text-left other-value">R$ {{number_format($lote->valor_arroba,2,',','.')}}</td>
                            <td class="text-left other-value">R$ {{number_format($lote->valor_animal,2,',','.')}}</td>
                            <td class="text-left other-value">R$ {{number_format($lote->custo,2,',','.')}}</td>
                            <td class="text-left other-value">R$ {{number_format($lote->custo_arroba,2,',','.')}}</td>
                            <td class="text-left other-value">R$ {{number_format($lote->custo_animal,2,',','.')}}</td>
                        </tr>
                        </tbody>
                        <thead>
                        <tr>
                            <th class="text-left">Comissão Total</th>
                            <th class="text-left">Comissão por Arroba</th>
                            <th class="text-left">Comissão por Animal</th>
                            <th class="text-left">Frete Total</th>
                            <th class="text-left">Frete Arroba</th>
                            <th class="text-left">Frete Animal</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="text-left value">R$ {{number_format($lote->comissao_total,2,',','.')}}</td>
                            <td class="text-left value">R$ {{number_format($lote->comissao_arroba,2,',','.')}}</td>
                            <td class="text-left value">R$ {{number_format($lote->comissao_animal,2,',','.')}}</td>
                            <td class="text-left value">R$ {{number_format($lote->valor_frete,2,',','.')}}</td>
                            <td class="text-left value">R$ {{number_format($lote->frete_arroba,2,',','.')}}</td>
                            <td class="text-left value">R$ {{number_format($lote->frete_animal,2,',','.')}}</td>
                        </tr>

                        @foreach($lote->tipos as $tipo)
                            @if($tipo['valor'] > 0)
                                <tr>
                                    <td class="text-center" colspan="6"><strong>Animais do
                                            tipo {{$tipo['label']}}</strong></td>
                                </tr>
                                <tr>
                                    <th class="text-left">Valor Total</th>
                                    <th class="text-left">Valor médio por arroba</th>
                                    <th class="text-left">Valor Médio por Animal</th>
                                    <th class="text-left">Custo Total</th>
                                    <th class="text-left">Custo Médio por Arroba</th>
                                    <th class="text-left">Custo Médio por Animal</th>
                                </tr>
                                <tr>
                                    <td class="text-left other-value" style="white-space: nowrap;">
                                        R$ {{number_format($tipo['valor'], 2, ',', '.')}}</td>
                                    <td class="text-left other-value">
                                        R$ {{number_format($tipo['valor_arroba'], 2, ',', '.')}}</td>
                                    <td class="text-left other-value">
                                        R$ {{number_format($tipo['valor_animal'], 2, ',', '.')}}</td>
                                    <td class="text-left other-value">R$ {{number_format($tipo['custo'], 2, ',', '.')}}</td>
                                    <td class="text-left other-value">
                                        R$ {{number_format($tipo['custo_arroba'], 2, ',', '.')}}</td>
                                    <td class="text-left other-value">
                                        R$ {{number_format($tipo['custo_animal'], 2, ',', '.')}}</td>
                                </tr>
                                <tr>
                                    <th class="text-left">Frete Total</th>
                                    <th class="text-left">Frete por Arroba</th>
                                    <th class="text-left">Frete por Animal</th>
                                    <th class="text-left">Total de Comissão</th>
                                    <th class="text-left">Comissão por Arroba</th>
                                    <th class="text-left">Comissão por Animal</th>
                                </tr>
                                <tr>
                                    <td class="text-left value">R$ {{number_format($tipo['frete'], 2, ',', '.')}}</td>
                                    <td class="text-left value">R$ {{number_format($tipo['frete_arroba'], 2, ',', '.')}}</td>
                                    <td class="text-left value">R$ {{number_format($tipo['frete_animal'], 2, ',', '.')}}</td>
                                    <td class="text-left value">R$ {{number_format($tipo['valor_total_comissao'], 2, ',', '.')}}</td>
                                    <td class="text-left value">R$ {{number_format($tipo['comissao_arroba'], 2, ',', '.')}}</td>
                                    <td class="text-left value">R$ {{number_format($tipo['comissao_animal'], 2, ',', '.')}}</td>
                                </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                </td>
            </tr>
        @endforeach
    </table>
</div>
<style>
    /* latin */
    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 400;
        src: local('Open Sans Regular'), local('OpenSans-Regular'), url({{asset('fonts/opensans400.woff2')}}) format('woff2');
        unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    }

    /* latin */
    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 600;
        src: local('Open Sans SemiBold'), local('OpenSans-SemiBold'), url({{asset('fonts/opensans600.woff2')}}) format('woff2');
        unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    }

    /* latin */
    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 700;
        src: local('Open Sans Bold'), local('OpenSans-Bold'), url({{asset('fonts/opensans700.woff2')}}) format('woff2');
        unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    }

    body {
        margin: 0;
        padding: 0;
        font-family: 'Open Sans', sans-serif;
    }

    .content {
        width: 85%;
        margin: auto;
        margin-top: 20px;
    }

    .title {
        text-align: center;
        font-weight: 600;
        font-size: 18px;
    }

    .content table {
        width: 100%;
        border-collapse: collapse;
    }

    .content > table {
        border: 0.5px solid #000;
    }

    .content > table > tr > td {
        padding: 5px;
    }


    .lotes table {
        border: 1px solid #BBBBBB;
    }

    .lotes > td td,
    .lotes > td th {
        padding: 1px 5px;
    }

    .lotes th {
        padding: 0;
        font-weight: 700;
    }

    .content > table tr td {
        font-size: 11px;
    }

    .text-center {
        text-align: center;
    }

    .text-left {
        text-align: left;
    }

    h3 {
        margin: 0;
        font-weight: 600;
    }

    .value {
        background: #d5d5d5;
    }

    .other-value {
        background: #e3e3e3;
    }


</style>

<style>
    @media print {
        .content {
            width: 100%;
            margin: 0;
        }

        .quebrar-pagina {
            height: 25px !important;
            display: table-cell !important;
        }
    }
</style>
<script>
    window.onload = function () {
        // window.print();
        // window.close();
    }
</script>
</body>
</html>
