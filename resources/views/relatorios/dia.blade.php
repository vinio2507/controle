@extends('template')

@section('content')
    <div class="container">
        <div class="row">
            <script>
                function imprimir() {
                    if (!$('#data_relatorio').val()) {
                        alert('Selecione uma data!');
                        $('#data_relatorio').focus();
                    } else {
                        window.open("{{url('relatorio/imprimir?data_relatorio=')}}" + $('#data_relatorio').val() + '&viewer=relatorios.imprimir');
                    }
                }

            </script>
            <form action="{{route('relatorio.dia')}}" method="GET" class="w-100">
                <div class="col-md-12">
                    <div class="page-header">
                        <h2>
                            Relatório do Dia
                            <a onclick="imprimir()" class="btn btn-primary float-right">Imprimir
                            </a>
                        </h2>
                        <hr>
                    </div>
                </div>
                <div class="col-md-12 mb-3">
                    <label for="data">Data</label>
                    <div class="form-inline">
                        <input value="{{$data?$data:date('Y-m-d')}}" id="data_relatorio" name="data_relatorio"
                               type="date"
                               class="form-control">
                        <button class="btn btn-success ml-3">Filtrar</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="row">
            <div class="col-md-12 panel-containers">
                @foreach($dados as $dado)
                    @if($dado['label'] === '#!#inicio#!#')
                        <div class="clearfix"></div>
                    @endif
                    @if($dado['label'] === '#!#section#!#')
                        <div class="clearfix"></div>
                        <div><h2 class="pt-4 pb-2">{{$dado['value']}}</h2></div>
                        <div class="clearfix"></div>
                    @endif
                    @if($dado['label'] !== '#!#inicio#!#' && $dado['label'] !== '#!#section#!#')
                        <div class="panel mr-md-3 mb-3">
                            <div class="panel-head">
                                <strong>{{$dado['label']}}</strong>
                            </div>
                            <div class="panel-body pt-2">
                                {{$dado['value']}}
                            </div>
                            @if(isset($dado['leg']))
                                <div class="panel-foot pb-2">
                                    {{$dado['leg']}}
                                </div>
                            @endif
                        </div>
                    @endif
                @endforeach
                <div class="clear"></div>
            </div>
            <div class="col-md-12 mb-3">
                <div class="page-header">
                    <h2>
                        Lotes Cadastrados
                    </h2>
                    <hr>
                </div>

                <table>
                    <thead>
                    <tr>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($lotes as $lote)
                        <tr class="lotes">
                            <td>
                                <table border=1>
                                    <thead>
                                    <tr>
                                        <td colspan="3"><strong>Proprietário: </strong>{{$lote->proprietario}}</td>
                                        <td colspan="3"><strong>Número do lote: </strong>{{$lote->numero}}</td>
                                    </tr>
                                    <tr>
                                        <th class="text-left">Valor Total</th>
                                        <th class="text-left">Valor Arroba</th>
                                        <th class="text-left">Valor Animal</th>
                                        <th class="text-left">Custo Total</th>
                                        <th class="text-left">Custo por Arroba</th>
                                        <th class="text-left">Custo por Animal</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="text-left other-value">
                                            R$ {{number_format($lote->valor_total,2,',','.')}}</td>
                                        <td class="text-left other-value">
                                            R$ {{number_format($lote->valor_arroba,2,',','.')}}</td>
                                        <td class="text-left other-value">
                                            R$ {{number_format($lote->valor_animal,2,',','.')}}</td>
                                        <td class="text-left other-value">
                                            R$ {{number_format($lote->custo,2,',','.')}}</td>
                                        <td class="text-left other-value">
                                            R$ {{number_format($lote->custo_arroba,2,',','.')}}</td>
                                        <td class="text-left other-value">
                                            R$ {{number_format($lote->custo_animal,2,',','.')}}</td>
                                    </tr>
                                    </tbody>
                                    <thead>
                                    <tr>
                                        <th class="text-left">Comissão Total</th>
                                        <th class="text-left">Comissão por Arroba</th>
                                        <th class="text-left">Comissão por Animal</th>
                                        <th class="text-left">Frete Total</th>
                                        <th class="text-left">Frete Arroba</th>
                                        <th class="text-left">Frete Animal</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="text-left value">
                                            R$ {{number_format($lote->comissao_total,2,',','.')}}</td>
                                        <td class="text-left value">
                                            R$ {{number_format($lote->comissao_arroba,2,',','.')}}</td>
                                        <td class="text-left value">
                                            R$ {{number_format($lote->comissao_animal,2,',','.')}}</td>
                                        <td class="text-left value">
                                            R$ {{number_format($lote->valor_frete,2,',','.')}}</td>
                                        <td class="text-left value">
                                            R$ {{number_format($lote->frete_arroba,2,',','.')}}</td>
                                        <td class="text-left value">
                                            R$ {{number_format($lote->frete_animal,2,',','.')}}</td>
                                    </tr>
                                    </tbody>

                                </table>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="9">Nenhum lote cadastrado!</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>

            </div>
        </div>
    </div>
    <style>
        .panel-containers {
            margin-bottom: 15px;
        }

        .panel {
            display: block;
            width: 290px;
            background: #F1F1F1;
            float: left;
            border-radius: 3px;
            box-shadow: 0px 1px 4px #999;
            height: 130px;
        }

        .panel-head {
            background: #F1F1F1;
            border-top-left-radius: 3px;
            border-top-right-radius: 3px;
            border-bottom: 1px solid #DFDFE0;
            box-shadow: 0 0.5px 0.5px #cccccc;
            padding: 4px 10px;
            text-align: center;
            font-size: 15px;
        }

        .panel-body {
            text-align: center;
            font-size: 27px;
            font-weight: bold;
        }

        .panel-foot {
            text-align: center;
            font-size: 23px;
        }

        @media (min-width: 992px) {
            .panel {
                width: calc(25% - 16px);
            }

        }


        /* latin */
        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 400;
            src: local('Open Sans Regular'), local('OpenSans-Regular'), url({{asset('fonts/opensans400.woff2')}}) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* latin */
        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 600;
            src: local('Open Sans SemiBold'), local('OpenSans-SemiBold'), url({{asset('fonts/opensans600.woff2')}}) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* latin */
        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 700;
            src: local('Open Sans Bold'), local('OpenSans-Bold'), url({{asset('fonts/opensans700.woff2')}}) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        body {
            margin: 0;
            padding: 0;
            font-family: 'Open Sans', sans-serif;
        }

        .content {
            width: 85%;
            margin: auto;
            margin-top: 20px;
        }

        .title {
            text-align: center;
            font-weight: 600;
            font-size: 18px;
        }

        .content table {
            width: 100%;
            border-collapse: collapse;
        }

        .content > table {
            border: 0.5px solid #000;
        }

        .content > table > tr > td {
            padding: 5px;
        }


        .lotes table {
            border: 1px solid #BBBBBB;
        }

        .lotes > td td,
        .lotes > td th {
            padding: 1px 5px;
        }

        .lotes th {
            padding: 0;
            font-weight: 700;
        }

        .content > table tr td {
            font-size: 11px;
        }

        .text-center {
            text-align: center;
        }

        .text-left {
            text-align: left;
        }

        h3 {
            margin: 0;
            font-weight: 600;
        }

        .value {
            background: #d5d5d5;
        }

        .other-value {
            background: #e3e3e3;
        }

        table {
            width: 100%;
        }

        .lotes > td {
            padding-bottom: 15px;
        }
    </style>
@endsection
