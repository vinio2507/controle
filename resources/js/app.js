/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

// window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// const app = new Vue({
//     el: '#app'
// });

$(function () {
    function contarAnimais() {
        let animais_total = 0;
        $('.animal_quantidade').each((index, item) => {
            animais_total += parseInt($(item).val());
        });
        return parseInt(animais_total);
    }

    // function calcularQuantidadeFrete() {

    // let tipo_frete = parseInt($('#tipo_frete').val());
    // if (tipo_frete) {
    //     let qtd = parseInt($('#tipo_frete option[value="' + tipo_frete + '"]').attr('qtd'));
    //     let animais = contarAnimais();
    //     let fretes = parseInt(animais / qtd);
    //     if (animais % qtd > 0) {
    //         fretes++;
    //     }
    //
    //     return fretes;
    // }
    // return '';
    // }

    function contarAnimaisFrete() {
        let animais = 0;
        $('.qtd_animais_frete').each((index, item) => {
            animais += parseFloat($(item).val());
        });
        return animais;
    }

    function calcularTotalFrete() {
        let total = 0.0;
        $('.frete_valor').each((index, item) => {
            total += parseFloat($(item).val());
        });

        $('#total_animais_frete').val(contarAnimaisFrete());
        $('#total_animais_frete_disabled').val(contarAnimaisFrete());
        return total.toFixed(2);
    }

    function calcularFrete(distancia, tipo) {
        let valor = 0;


        let ffaixas = faixas.filter(item => {
            return parseInt(item.tipo) === parseInt(tipo);
        });

        ffaixas.forEach(item => {
            if (parseInt(item.faixa_2) === 0 && parseInt(distancia) > parseInt(item.faixa_1)) {
                valor = item.valor * distancia;
            }
            if (item.faixa_2 && parseInt(item.faixa_2) > 0) {
                if (distancia >= item.faixa_1 && distancia <= item.faixa_2) {
                    valor = item.valor;
                }
            }
        });

        console.log(valor);
        valor = parseFloat(valor);
        return valor.toFixed(2);
    }

    $('#tipo_frete').change(event => {
        $('#qtd_animais').val($(event.target).find('option:selected').attr('qtd'));
        $('#qtd_animais').attr('max', $(event.target).find('option:selected').attr('qtd'));
    });


    $('.remover-animal').on('click', event => {
        $(event.target).parent().parent().remove();
    });


    $('#adicionar_frete').on('click', () => {
        let distancia = parseFloat($('#distancia').val());
        let tipo = parseInt($('#tipo_frete').val());
        let tipo_label = $(`#tipo_frete option[value="${tipo}"]`).text();
        let qtdAnimais = parseInt($('#qtd_animais').val());

        let valor = calcularFrete(distancia, tipo);

        let html = '<tr>\
            <td>' + distancia + '<input name="frete_distancia[]" type="hidden" value="' + distancia + '"></td>\
            <td>' + tipo_label + '<input name="frete_tipo[]" type="hidden" value="' + tipo + '"></td>\
            <td>' + qtdAnimais + '<input class="qtd_animais_frete" name="frete_qtd_animais[]" type="hidden" value="' + qtdAnimais + '"></td>\
            <td>' + valor + '<input class="frete_valor" name="frete_valor[]" type="hidden" value="' + valor + '"></td>\
            <td><a class="remover-frete btn btn-danger btn-sm">Remover</a></td>\
        </tr>';

        if (distancia && tipo && qtdAnimais)
            $('.tabela-fretes table tbody').append(html);

        $('#valor_total_frete').val(calcularTotalFrete());
        $('#valor_total_frete_disabled').val(calcularTotalFrete());

        $('.remover-frete').off('click');
        $('.remover-frete').on('click', event => {
            $(event.target).parent().parent().remove();
            $('#valor_total_frete').val(calcularTotalFrete());
            $('#valor_total_frete_disabled').val(calcularTotalFrete());
        });
    });


    $('#adicionar_animal').on('click', () => {
            let qtd = $('#quantidade_animal').val();
            let tipo = $('#tipo_animal').val();
            let tipo_label = $(`#tipo_animal option[value="${tipo}"]`).text();
            let peso = $('#peso_total').val();
            let valor = $('#valor_arroba').val();
            let html = '<tr>\n' +
                '                                <td>' + qtd + '<input value="' + qtd + '" class="animal_quantidade" type="hidden" name="animal_quantidade[]"></td>\n' +
                '                                <td>' + tipo_label + '<input value="' + tipo + '" type="hidden" name="animal_tipo[]"></td>\n' +
                '                                <td>' + peso + ' KG<input value="' + peso + '" type="hidden" name="animal_peso[]"></td>\n' +
                '                                <td>R$ ' + valor + '<input value="' + valor + '" type="hidden" name="animal_valor_arroba[]"></td>\n' +
                '                                <td class="text-center">\n' +
                '                                    <a class="remover-animal btn btn-danger btn-sm">Remover</a>\n' +
                '                                </td>\n' +
                '                            </tr>';

            if (qtd && tipo && peso && valor)
                $('.tabela-animais table tbody').append(html);

            $('#total_animais').val(contarAnimais());
            $('#total_animais_disabled').val(contarAnimais());

            $('.remover-animal').off('click');
            $('.remover-animal').on('click', event => {
                $(event.target).parent().parent().remove();
                $('#total_animais').val(contarAnimais());
                $('#total_animais_disabled').val(contarAnimais());
            });
        }
    );
});
