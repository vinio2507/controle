<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\TipoAnimal;

Route::get('/', 'ControleController@index');

Route::resource('controle','ControleController');
Route::resource('tipoanimal','TipoAnimalController');
Route::resource('tipofrete','TipoFreteController');
Route::resource('faixafrete','FaixaFreteController');
Route::get('relatorio/geral','RelatorioController@geral')->name('relatorio.geral');
Route::get('relatorio/dia','RelatorioController@dia')->name('relatorio.dia');
Route::get('relatorio/imprimir','RelatorioController@imprimir')->name('relatorio.imprimir');
