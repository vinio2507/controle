::[Bat To Exe Converter]
::
::YAwzoRdxOk+EWAjk
::fBw5plQjdCyDJGyX8VAjFA1dQg2NAE+1EbsQ5+n//NaSskkcQOdycYzU1PqHI+9z
::YAwzuBVtJxjWCl3EqQJgSA==
::ZR4luwNxJguZRRnk
::Yhs/ulQjdF+5
::cxAkpRVqdFKZSTk=
::cBs/ulQjdF+5
::ZR41oxFsdFKZSDk=
::eBoioBt6dFKZSDk=
::cRo6pxp7LAbNWATEpCI=
::egkzugNsPRvcWATEpCI=
::dAsiuh18IRvcCxnZtBJQ
::cRYluBh/LU+EWAnk
::YxY4rhs+aU+IeA==
::cxY6rQJ7JhzQF1fEqQJhZksaHErSXA==
::ZQ05rAF9IBncCkqN+0xwdVsFAlTMbCXqZg==
::ZQ05rAF9IAHYFVzEqQIcKQlVWgGOfF6qArQI7fqb
::eg0/rx1wNQPfEVWB+kM9LVsJDBGSOGquA6dS7fD+jw==
::fBEirQZwNQPfEVWB+kM9LVsJDBGSOGquA6d8
::cRolqwZ3JBvQF1fEqQIRaA5ESAWWOXn6B6UM5OH47v6OrUNdQO1/dI7J26KLLKAD+ErucJU5xRo=
::dhA7uBVwLU+EWFyj+E0yKRc0
::YQ03rBFzNR3SWATElA==
::dhAmsQZ3MwfNWATEwks+IRhdWRfCHm6oErpcSiG7vbrW+y0=
::ZQ0/vhVqMQ3MEVWAtB9wSA==
::Zg8zqx1/OA3MEVWAtB9wSA==
::dhA7pRFwIByZRRnk
::Zh4grVQjdCyDJGyX8VAjFA1dQg2NAE+/Fb4I5/jH3/iIqEgJW/EDbZ/f26CLMq4W8kCE
::YB416Ek+ZG8=
::
::
::978f952a14a936cc963da21a135fa983
@echo off
mode 60,10

title Atualizacao do Sistema

del log.txt

cls
color 1F
ECHO Testando conexao com a internet...
ping -n 1 8.8.8.8
cls
TYPE nul > log.txt
ECHO 01 ---- Testando conexão com a internet... >> log.txt
IF NOT ERRORLEVEL 1 goto OK
IF ERRORLEVEL 1 goto ERRO


:OK
cls
ECHO Internet OK!
ECHO:
ECHO 02 ---- Internet OK! >> log.txt
ECHO Verificando a existencia de atualizacoes para o sistema...
ECHO 03 ---- Verificando a existência de atualizações para o sistema >> log.txt
cmd /c "git pull > git.log"
for /f %%a in ('type git.log ^| find /c /i "up to date"') do set err=%%a
del git.log
if "%err%" GTR "0" goto SAIR_GIT
IF ERRORLEVEL 1 goto ERRO_GIT
IF NOT ERRORLEVEL 1 goto GIT_SUCCESS


:RUN_NPM
cls
ECHO Instalando os pacotes do npm...
ECHO 05 ---- Instalando os pacotes do npm >> log.txt
cmd /c "npm install > install.log 2>&1"
for /f %%a in ('type install.log ^| find /c /i "err"') do set err=%%a
if "%err%" GTR "0" goto ERRO_NPM
IF ERRORLEVEL 1 goto ERRO_NPM
IF NOT ERRORLEVEL 1 goto NPM_SUCCESS
goto SAIR

:GIT_SUCCESS
cls
ECHO Download da atualizacao realizado com sucesso!
ECHO 04 ---- Download da atualização realizado com sucesso!>> log.txt
GOTO RUN_NPM


:NPM_SUCCESS
cls
del install.log
ECHO Pacotes do npm instalados com sucesso...
ECHO 06 ---- Pacotes npm instalados com sucesso>> log.txt
cls
ECHO Instalando os pacotes do Composer
ECHO 07 ---- Instalando os pacotes do Composer>> log.txt
cmd /c "composer install > install.log 2>&1"
IF ERRORLEVEL 1 goto ERRO_COMPOSER
IF NOT ERRORLEVEL 1 goto COMPOSER_SUCCESS

:COMPOSER_SUCCESS
cls
del install.log
ECHO Pacotes do Composer instalados com sucesso...
ECHO 08 ---- Pacotes do Composer instalados com sucesso>> log.txt
cls
ECHO Atualizando banco de dados do sistema...
ECHO 09 ---- Atualizando banco de dados do sistema...>> log.txt

cmd /c "php artisan migrate"

IF ERRORLEVEL 1 goto ERRO_BANCO
IF NOT ERRORLEVEL 1 goto BANCO_SUCCESS

:BANCO_SUCCESS
ECHO Banco de dados atualizado com sucesso...
ECHO 10 ---- Banco de dados atualizado com sucesso...>> log.txt
cls
ECHO Atualizacao concluida com sucesso!
ECHO Atualização concluída com sucesso!>> log.txt
GOTO SAIR


:ERRO_BANCO
cls
ECHO Erro, não foi possível atualizar o banco de dados do sistema. Entre em contato com o desenvolvedor.>> log.txt
ECHO:
ECHO Falha ao atualizar o banco de dados do sistema. Para mais detalhes verifique o arquivo log.txt!
GOTO SAIR

:ERRO_COMPOSER
ECHO Erro, não foi possível instalar os pacotes do Composer da atualização. Entre em contato com o desenvolvedor.>> log.txt
ECHO:
ECHO Falha ao instalar os pacotes do Composer da atualizacao do sistema. Para mais detalhes verifique o arquivo log.txt!
del install.log
GOTO SAIR

:ERRO
cls
ECHO Erro, para mais detalhes verifique o arquivo log.txt!
ECHO Erro ---- Falha ao se conectar com a internet! >> log.txt
ECHO O sistema não pode ser atualizado, pois não foi possível conectar-se com a internet! >> log.txt
GOTO SAIR

:ERRO_GIT
cls
ECHO Erro ao se conectar com o servidor de atualizacao!
ECHO Para mais detalhes verifique o arquivo log.txt!
ECHO Erro, não foi posível se conectar com o servidor de atualização! >> log.txt
ECHO: >> log.txt
ECHO Tente novamente, se o erro persistir faça uma solicitação de atendimento >> log.txt
ECHO para o desenvolvedor! >> log.txt
GOTO SAIR

:ERRO_NPM
ECHO Erro, não foi possível instalar os pacotes npm da atualização. Entre em contato com o desenvolvedor.>> log.txt
ECHO:
ECHO Falha ao instalar os pacotes npm da atualizacao do sistema. Para mais detalhes verifique o arquivo log.txt!
del install.log
GOTO SAIR

:SAIR_GIT
ECHO O sistema esta atualizado!
ECHO O sistema esta atualizado! >> log.txt

:SAIR
